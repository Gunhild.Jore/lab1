package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        System.out.println("Let's play round " + roundCounter++);

		
		startover:
		
		while(true){
			
			System.out.println("Your choice (Rock/Paper/Scissors)? ");
			String myMove = sc.nextLine();
			
			
			if(!myMove.equals("rock") && !myMove.equals("paper") && !myMove.equals("scissors")) {
			System.out.println("Your move isnt valid. Try again.");
			}else {
				
				
			int rand = (int)(Math.random() * 3);
			String oppMove = "";
			if (rand == 0) {
				oppMove = "rock";
			}else if(rand == 1) {
				oppMove = "paper";
			}else {
				oppMove = "scissors";
				
			}
		 System.out.print("Human chose "+ myMove + ", computer chose " + oppMove+". ");
		 
		 
		 if(myMove.equals(oppMove)) {
			 System.out.println("It's a tie!");
		 }else if((myMove.equals("rock") && oppMove.equals
				 ("Scissors")) || (myMove.equals("scissors") 
				&& oppMove.equals("paper")) || 
				 (myMove.equals("paper") && oppMove.equals("rock"))) {
			 System.out.println("Human won");
			 humanScore++;
		}else {
			System.out.println("Computer won");
			computerScore++;
		}
		 System.out.println("Score: human " +humanScore + ", computer "+ computerScore);
	 }
			while(true) {
				System.out.println("Do you wish to continue playing? (y/n)? ");
				String newRound = sc.nextLine();
				if(newRound.equals("y")) {
					System.out.println("Let's play round " + roundCounter++ );
					break;
				}else if(!newRound.equals("y") && !newRound.equals("n")) {
					System.out.println("I dont understand" + newRound + ".Try again.");
				}else {
					System.out.println("Bye bye :) ");
				    break startover;
					}
				}
            }

        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
